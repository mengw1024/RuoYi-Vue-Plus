package org.dromara.wms.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.wms.domain.vo.BasicsAreaVo;
import org.dromara.wms.domain.bo.BasicsAreaBo;
import org.dromara.wms.service.IBasicsAreaService;
import org.dromara.common.mybatis.core.page.TableDataInfo;

/**
 * 库区
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/wms/area")
public class BasicsAreaController extends BaseController {

    private final IBasicsAreaService basicsAreaService;

    /**
     * 查询库区列表
     */
    @SaCheckPermission("wms:area:list")
    @GetMapping("/list")
    public TableDataInfo<BasicsAreaVo> list(BasicsAreaBo bo, PageQuery pageQuery) {
        return basicsAreaService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出库区列表
     */
    @SaCheckPermission("wms:area:export")
    @Log(title = "库区", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(BasicsAreaBo bo, HttpServletResponse response) {
        List<BasicsAreaVo> list = basicsAreaService.queryList(bo);
        ExcelUtil.exportExcel(list, "库区", BasicsAreaVo.class, response);
    }

    /**
     * 获取库区详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("wms:area:query")
    @GetMapping("/{id}")
    public R<BasicsAreaVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(basicsAreaService.queryById(id));
    }

    /**
     * 新增库区
     */
    @SaCheckPermission("wms:area:add")
    @Log(title = "库区", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody BasicsAreaBo bo) {
        return toAjax(basicsAreaService.insertByBo(bo));
    }

    /**
     * 修改库区
     */
    @SaCheckPermission("wms:area:edit")
    @Log(title = "库区", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody BasicsAreaBo bo) {
        return toAjax(basicsAreaService.updateByBo(bo));
    }

    /**
     * 删除库区
     *
     * @param ids 主键串
     */
    @SaCheckPermission("wms:area:remove")
    @Log(title = "库区", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(basicsAreaService.deleteWithValidByIds(List.of(ids), true));
    }
}
