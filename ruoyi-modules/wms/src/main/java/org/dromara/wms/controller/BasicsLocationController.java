package org.dromara.wms.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.wms.domain.vo.BasicsLocationVo;
import org.dromara.wms.domain.bo.BasicsLocationBo;
import org.dromara.wms.service.IBasicsLocationService;
import org.dromara.common.mybatis.core.page.TableDataInfo;

/**
 * 货位
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/wms/location")
public class BasicsLocationController extends BaseController {

    private final IBasicsLocationService basicsLocationService;

    /**
     * 查询货位列表
     */
    @SaCheckPermission("wms:location:list")
    @GetMapping("/list")
    public TableDataInfo<BasicsLocationVo> list(BasicsLocationBo bo, PageQuery pageQuery) {
        return basicsLocationService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出货位列表
     */
    @SaCheckPermission("wms:location:export")
    @Log(title = "货位", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(BasicsLocationBo bo, HttpServletResponse response) {
        List<BasicsLocationVo> list = basicsLocationService.queryList(bo);
        ExcelUtil.exportExcel(list, "货位", BasicsLocationVo.class, response);
    }

    /**
     * 获取货位详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("wms:location:query")
    @GetMapping("/{id}")
    public R<BasicsLocationVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(basicsLocationService.queryById(id));
    }

    /**
     * 新增货位
     */
    @SaCheckPermission("wms:location:add")
    @Log(title = "货位", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody BasicsLocationBo bo) {
        return toAjax(basicsLocationService.insertByBo(bo));
    }

    /**
     * 修改货位
     */
    @SaCheckPermission("wms:location:edit")
    @Log(title = "货位", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody BasicsLocationBo bo) {
        return toAjax(basicsLocationService.updateByBo(bo));
    }

    /**
     * 删除货位
     *
     * @param ids 主键串
     */
    @SaCheckPermission("wms:location:remove")
    @Log(title = "货位", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(basicsLocationService.deleteWithValidByIds(List.of(ids), true));
    }
}
