package org.dromara.wms.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.wms.domain.vo.BasicsRackVo;
import org.dromara.wms.domain.bo.BasicsRackBo;
import org.dromara.wms.service.IBasicsRackService;
import org.dromara.common.mybatis.core.page.TableDataInfo;

/**
 * 货架
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/wms/rack")
public class BasicsRackController extends BaseController {

    private final IBasicsRackService basicsRackService;

    /**
     * 查询货架列表
     */
    @SaCheckPermission("wms:rack:list")
    @GetMapping("/list")
    public TableDataInfo<BasicsRackVo> list(BasicsRackBo bo, PageQuery pageQuery) {
        return basicsRackService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出货架列表
     */
    @SaCheckPermission("wms:rack:export")
    @Log(title = "货架", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(BasicsRackBo bo, HttpServletResponse response) {
        List<BasicsRackVo> list = basicsRackService.queryList(bo);
        ExcelUtil.exportExcel(list, "货架", BasicsRackVo.class, response);
    }

    /**
     * 获取货架详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("wms:rack:query")
    @GetMapping("/{id}")
    public R<BasicsRackVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(basicsRackService.queryById(id));
    }

    /**
     * 新增货架
     */
    @SaCheckPermission("wms:rack:add")
    @Log(title = "货架", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody BasicsRackBo bo) {
        return toAjax(basicsRackService.insertByBo(bo));
    }

    /**
     * 修改货架
     */
    @SaCheckPermission("wms:rack:edit")
    @Log(title = "货架", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody BasicsRackBo bo) {
        return toAjax(basicsRackService.updateByBo(bo));
    }

    /**
     * 删除货架
     *
     * @param ids 主键串
     */
    @SaCheckPermission("wms:rack:remove")
    @Log(title = "货架", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(basicsRackService.deleteWithValidByIds(List.of(ids), true));
    }
}
