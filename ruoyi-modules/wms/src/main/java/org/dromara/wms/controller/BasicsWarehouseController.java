package org.dromara.wms.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.wms.domain.vo.BasicsWarehouseVo;
import org.dromara.wms.domain.bo.BasicsWarehouseBo;
import org.dromara.wms.service.IBasicsWarehouseService;
import org.dromara.common.mybatis.core.page.TableDataInfo;

/**
 * 仓库信息
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/wms/warehouse")
public class BasicsWarehouseController extends BaseController {

    private final IBasicsWarehouseService basicsWarehouseService;

    /**
     * 查询仓库信息列表
     */
    @SaCheckPermission("wms:warehouse:list")
    @GetMapping("/list")
    public TableDataInfo<BasicsWarehouseVo> list(BasicsWarehouseBo bo, PageQuery pageQuery) {
        return basicsWarehouseService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出仓库信息列表
     */
    @SaCheckPermission("wms:warehouse:export")
    @Log(title = "仓库信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(BasicsWarehouseBo bo, HttpServletResponse response) {
        List<BasicsWarehouseVo> list = basicsWarehouseService.queryList(bo);
        ExcelUtil.exportExcel(list, "仓库信息", BasicsWarehouseVo.class, response);
    }

    /**
     * 获取仓库信息详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("wms:warehouse:query")
    @GetMapping("/{id}")
    public R<BasicsWarehouseVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(basicsWarehouseService.queryById(id));
    }

    /**
     * 新增仓库信息
     */
    @SaCheckPermission("wms:warehouse:add")
    @Log(title = "仓库信息", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody BasicsWarehouseBo bo) {
        return toAjax(basicsWarehouseService.insertByBo(bo));
    }

    /**
     * 修改仓库信息
     */
    @SaCheckPermission("wms:warehouse:edit")
    @Log(title = "仓库信息", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody BasicsWarehouseBo bo) {
        return toAjax(basicsWarehouseService.updateByBo(bo));
    }

    /**
     * 删除仓库信息
     *
     * @param ids 主键串
     */
    @SaCheckPermission("wms:warehouse:remove")
    @Log(title = "仓库信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(basicsWarehouseService.deleteWithValidByIds(List.of(ids), true));
    }
}
