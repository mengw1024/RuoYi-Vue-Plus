package org.dromara.wms.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.wms.domain.vo.BrandsVo;
import org.dromara.wms.domain.bo.BrandsBo;
import org.dromara.wms.service.IBrandsService;
import org.dromara.common.mybatis.core.page.TableDataInfo;

/**
 * 品牌
 *
 * @author Lion Li
 * @date 2024-01-11
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/wms/brands")
public class BrandsController extends BaseController {

    private final IBrandsService brandsService;

    /**
     * 查询品牌列表
     */
    @SaCheckPermission("wms:brands:list")
    @GetMapping("/list")
    public TableDataInfo<BrandsVo> list(BrandsBo bo, PageQuery pageQuery) {
        return brandsService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出品牌列表
     */
    @SaCheckPermission("wms:brands:export")
    @Log(title = "品牌", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(BrandsBo bo, HttpServletResponse response) {
        List<BrandsVo> list = brandsService.queryList(bo);
        ExcelUtil.exportExcel(list, "品牌", BrandsVo.class, response);
    }

    /**
     * 获取品牌详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("wms:brands:query")
    @GetMapping("/{id}")
    public R<BrandsVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(brandsService.queryById(id));
    }

    /**
     * 新增品牌
     */
    @SaCheckPermission("wms:brands:add")
    @Log(title = "品牌", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody BrandsBo bo) {
        return toAjax(brandsService.insertByBo(bo));
    }

    /**
     * 修改品牌
     */
    @SaCheckPermission("wms:brands:edit")
    @Log(title = "品牌", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody BrandsBo bo) {
        return toAjax(brandsService.updateByBo(bo));
    }

    /**
     * 删除品牌
     *
     * @param ids 主键串
     */
    @SaCheckPermission("wms:brands:remove")
    @Log(title = "品牌", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(brandsService.deleteWithValidByIds(List.of(ids), true));
    }
}
