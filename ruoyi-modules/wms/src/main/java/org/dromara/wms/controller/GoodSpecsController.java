package org.dromara.wms.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.wms.domain.vo.GoodSpecsVo;
import org.dromara.wms.domain.bo.GoodSpecsBo;
import org.dromara.wms.service.IGoodSpecsService;
import org.dromara.common.mybatis.core.page.TableDataInfo;

/**
 * 商品规格
 *
 * @author Lion Li
 * @date 2024-01-11
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/wms/specs")
public class GoodSpecsController extends BaseController {

    private final IGoodSpecsService goodSpecsService;

    /**
     * 查询商品规格列表
     */
    @SaCheckPermission("wms:specs:list")
    @GetMapping("/list")
    public TableDataInfo<GoodSpecsVo> list(GoodSpecsBo bo, PageQuery pageQuery) {
        return goodSpecsService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出商品规格列表
     */
    @SaCheckPermission("wms:specs:export")
    @Log(title = "商品规格", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(GoodSpecsBo bo, HttpServletResponse response) {
        List<GoodSpecsVo> list = goodSpecsService.queryList(bo);
        ExcelUtil.exportExcel(list, "商品规格", GoodSpecsVo.class, response);
    }

    /**
     * 获取商品规格详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("wms:specs:query")
    @GetMapping("/{id}")
    public R<GoodSpecsVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(goodSpecsService.queryById(id));
    }

    /**
     * 新增商品规格
     */
    @SaCheckPermission("wms:specs:add")
    @Log(title = "商品规格", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody GoodSpecsBo bo) {
        return toAjax(goodSpecsService.insertByBo(bo));
    }

    /**
     * 修改商品规格
     */
    @SaCheckPermission("wms:specs:edit")
    @Log(title = "商品规格", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody GoodSpecsBo bo) {
        return toAjax(goodSpecsService.updateByBo(bo));
    }

    /**
     * 删除商品规格
     *
     * @param ids 主键串
     */
    @SaCheckPermission("wms:specs:remove")
    @Log(title = "商品规格", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(goodSpecsService.deleteWithValidByIds(List.of(ids), true));
    }
}
