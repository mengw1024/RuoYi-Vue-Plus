package org.dromara.wms.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.wms.domain.vo.GoodsVo;
import org.dromara.wms.domain.bo.GoodsBo;
import org.dromara.wms.service.IGoodsService;
import org.dromara.common.mybatis.core.page.TableDataInfo;

/**
 * 商品
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/wms/goods")
public class GoodsController extends BaseController {

    private final IGoodsService goodsService;

    /**
     * 查询商品列表
     */
    @SaCheckPermission("wms:goods:list")
    @GetMapping("/list")
    public TableDataInfo<GoodsVo> list(GoodsBo bo, PageQuery pageQuery) {
        return goodsService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出商品列表
     */
    @SaCheckPermission("wms:goods:export")
    @Log(title = "商品", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(GoodsBo bo, HttpServletResponse response) {
        List<GoodsVo> list = goodsService.queryList(bo);
        ExcelUtil.exportExcel(list, "商品", GoodsVo.class, response);
    }

    /**
     * 获取商品详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("wms:goods:query")
    @GetMapping("/{id}")
    public R<GoodsVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(goodsService.queryById(id));
    }

    /**
     * 新增商品
     */
    @SaCheckPermission("wms:goods:add")
    @Log(title = "商品", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody GoodsBo bo) {
        return toAjax(goodsService.insertByBo(bo));
    }

    /**
     * 修改商品
     */
    @SaCheckPermission("wms:goods:edit")
    @Log(title = "商品", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody GoodsBo bo) {
        return toAjax(goodsService.updateByBo(bo));
    }

    /**
     * 删除商品
     *
     * @param ids 主键串
     */
    @SaCheckPermission("wms:goods:remove")
    @Log(title = "商品", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(goodsService.deleteWithValidByIds(List.of(ids), true));
    }
}
