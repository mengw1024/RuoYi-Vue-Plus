package org.dromara.wms.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.wms.domain.vo.PurchaseOrderVo;
import org.dromara.wms.domain.bo.PurchaseOrderBo;
import org.dromara.wms.service.IPurchaseOrderService;
import org.dromara.common.mybatis.core.page.TableDataInfo;

/**
 * 采购订单
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/wms/purchaseOrder")
public class PurchaseOrderController extends BaseController {

    private final IPurchaseOrderService purchaseOrderService;

    /**
     * 查询采购订单列表
     */
    @SaCheckPermission("wms:purchaseOrder:list")
    @GetMapping("/list")
    public TableDataInfo<PurchaseOrderVo> list(PurchaseOrderBo bo, PageQuery pageQuery) {
        return purchaseOrderService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出采购订单列表
     */
    @SaCheckPermission("wms:purchaseOrder:export")
    @Log(title = "采购订单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(PurchaseOrderBo bo, HttpServletResponse response) {
        List<PurchaseOrderVo> list = purchaseOrderService.queryList(bo);
        ExcelUtil.exportExcel(list, "采购订单", PurchaseOrderVo.class, response);
    }

    /**
     * 获取采购订单详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("wms:purchaseOrder:query")
    @GetMapping("/{id}")
    public R<PurchaseOrderVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(purchaseOrderService.queryById(id));
    }

    /**
     * 新增采购订单
     */
    @SaCheckPermission("wms:purchaseOrder:add")
    @Log(title = "采购订单", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody PurchaseOrderBo bo) {
        return toAjax(purchaseOrderService.insertByBo(bo));
    }

    /**
     * 修改采购订单
     */
    @SaCheckPermission("wms:purchaseOrder:edit")
    @Log(title = "采购订单", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody PurchaseOrderBo bo) {
        return toAjax(purchaseOrderService.updateByBo(bo));
    }

    /**
     * 删除采购订单
     *
     * @param ids 主键串
     */
    @SaCheckPermission("wms:purchaseOrder:remove")
    @Log(title = "采购订单", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(purchaseOrderService.deleteWithValidByIds(List.of(ids), true));
    }
}
