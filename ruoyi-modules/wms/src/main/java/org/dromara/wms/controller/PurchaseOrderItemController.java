package org.dromara.wms.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.wms.domain.vo.PurchaseOrderItemVo;
import org.dromara.wms.domain.bo.PurchaseOrderItemBo;
import org.dromara.wms.service.IPurchaseOrderItemService;
import org.dromara.common.mybatis.core.page.TableDataInfo;

/**
 * 采购订单明细
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/wms/PurchaseOrderItem")
public class PurchaseOrderItemController extends BaseController {

    private final IPurchaseOrderItemService purchaseOrderItemService;

    /**
     * 查询采购订单明细列表
     */
    @SaCheckPermission("wms:PurchaseOrderItem:list")
    @GetMapping("/list")
    public TableDataInfo<PurchaseOrderItemVo> list(PurchaseOrderItemBo bo, PageQuery pageQuery) {
        return purchaseOrderItemService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出采购订单明细列表
     */
    @SaCheckPermission("wms:PurchaseOrderItem:export")
    @Log(title = "采购订单明细", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(PurchaseOrderItemBo bo, HttpServletResponse response) {
        List<PurchaseOrderItemVo> list = purchaseOrderItemService.queryList(bo);
        ExcelUtil.exportExcel(list, "采购订单明细", PurchaseOrderItemVo.class, response);
    }

    /**
     * 获取采购订单明细详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("wms:PurchaseOrderItem:query")
    @GetMapping("/{id}")
    public R<PurchaseOrderItemVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(purchaseOrderItemService.queryById(id));
    }

    /**
     * 新增采购订单明细
     */
    @SaCheckPermission("wms:PurchaseOrderItem:add")
    @Log(title = "采购订单明细", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody PurchaseOrderItemBo bo) {
        return toAjax(purchaseOrderItemService.insertByBo(bo));
    }

    /**
     * 修改采购订单明细
     */
    @SaCheckPermission("wms:PurchaseOrderItem:edit")
    @Log(title = "采购订单明细", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody PurchaseOrderItemBo bo) {
        return toAjax(purchaseOrderItemService.updateByBo(bo));
    }

    /**
     * 删除采购订单明细
     *
     * @param ids 主键串
     */
    @SaCheckPermission("wms:PurchaseOrderItem:remove")
    @Log(title = "采购订单明细", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(purchaseOrderItemService.deleteWithValidByIds(List.of(ids), true));
    }
}
