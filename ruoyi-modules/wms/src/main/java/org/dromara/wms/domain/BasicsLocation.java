package org.dromara.wms.domain;

import org.dromara.common.tenant.core.TenantEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 货位对象 basics_location
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("basics_location")
public class BasicsLocation extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 仓库ID
     */
    private Long warehouseId;

    /**
     * 仓库编码
     */
    private String warehouseCode;

    /**
     * 库区ID
     */
    private Long areaId;

    /**
     * 库区编码
     */
    private String areaCode;

    /**
     * 货架ID
     */
    private Long rackId;

    /**
     * 货架编码
     */
    private String rackCode;

    /**
     * 货位编码
     */
    private String code;

    /**
     * 货位类型
     */
    private Long type;

    /**
     * ABC分类
     */
    private String abcType;

    /**
     * 重量上限
     */
    private Long weight;

    /**
     * 体积上限
     */
    private Long volume;

    /**
     * 补货触发量
     */
    private Long reorderPoint;

    /**
     * 补货上限
     */
    private Long restockCeiling;

    /**
     * 商品混放
     */
    private String commodityMixing;

    /**
     * 批次混放
     */
    private String batchMixing;

    /**
     * 排序
     */
    private Long sort;

    /**
     * 尺寸-长
     */
    private Long sizeLength;

    /**
     * 尺寸-宽
     */
    private Long sizeWidth;

    /**
     * 尺寸-高
     */
    private Long sizeHigh;

    /**
     * 排位层-排
     */
    private Long levelRow;

    /**
     * 排位层-位
     */
    private Long levelSlot;

    /**
     * 排位层-层
     */
    private Long levelTier;

    /**
     * 备注
     */
    private String remark;


}
