package org.dromara.wms.domain;

import org.dromara.common.tenant.core.TenantEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 仓库信息对象 basics_warehouse
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("basics_warehouse")
public class BasicsWarehouse extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 仓库编码
     */
    private String code;

    /**
     * 仓库名称
     */
    private String name;

    /**
     * 管理员ID
     */
    private String managerId;

    /**
     * 管理员姓名
     */
    private String managerName;

    /**
     * 联系地址
     */
    private String address;

    /**
     * 备注
     */
    private String remark;


}
