package org.dromara.wms.domain;

import org.dromara.common.tenant.core.TenantEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 品牌对象 good_brands
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("good_brands")
public class GoodBrands extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 品牌ID
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 品牌名
     */
    private String name;

    /**
     * 备注
     */
    private String remark;


}
