package org.dromara.wms.domain;

import org.dromara.common.tenant.core.TenantEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;

import java.io.Serial;

/**
 * 商品规格对象 good_sku
 *
 * @author Lion Li
 * @date 2024-01-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("good_sku")
public class GoodSku extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 商品ID
     */
    private Long goodId;

    /**
     * 规格编码
     */
    private String code;

    /**
     * 商品条码
     */
    private String barCode;

    /**
     * 颜色
     */
    private String color;

    /**
     * 重量
     */
    private Long weight;

    /**
     * 体积
     */
    private Long volume;

    /**
     * 长
     */
    private Long length;

    /**
     * 宽
     */
    private Long wide;

    /**
     * 高
     */
    private Long high;

    /**
     * 售价
     */
    private BigDecimal sellingPrice;

    /**
     * 批发价
     */
    private BigDecimal tradePrice;

    /**
     * 进价
     */
    private BigDecimal purchasePrice;

    /**
     * 备注
     */
    private String remark;


}
