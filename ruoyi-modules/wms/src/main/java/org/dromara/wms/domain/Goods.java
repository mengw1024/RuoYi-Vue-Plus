package org.dromara.wms.domain;

import org.dromara.common.tenant.core.TenantEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.util.List;

/**
 * 商品对象 goods
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("goods")
public class Goods extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 商品编码
     */
    private String code;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 品牌ID
     */
    private Long brandId;

    /**
     * 单位
     */
    private String unit;

    /**
     * 商品标记
     */
    private String sign;

    /**
     * 货号
     */
    private String number;

    /**
     * 备注
     */
    private String remark;

    /*
    * sku
    * */
    private List<GoodSku> goodSkuList;
}
