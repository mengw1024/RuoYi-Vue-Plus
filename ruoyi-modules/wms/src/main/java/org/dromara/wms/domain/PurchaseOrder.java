package org.dromara.wms.domain;

import org.dromara.common.tenant.core.TenantEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serial;

/**
 * 采购订单对象 purchase_order
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("purchase_order")
public class PurchaseOrder extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 业务时间
     */
    private Date businessTime;

    /**
     * 单据编号
     */
    private String code;

    /**
     * 供应商ID
     */
    private Long supplierManagementId;

    /**
     * 业务员
     */
    private Long salesmanId;

    /**
     * 收获仓库ID
     */
    private Long warehouseId;

    /**
     * 状态
     */
    private String status;

    /**
     * 商品总金额
     */
    private BigDecimal goodsTotalMoney;

    /**
     * 采购运费
     */
    private BigDecimal purchaseFreight;

    /**
     * 其他费用
     */
    private BigDecimal otherCost;

    /**
     * 预付金额
     */
    private BigDecimal amountAdvance;

    /**
     * 物流单号
     */
    private String trackNumber;

    /**
     * 总采购数量
     */
    private Long totalPurchaseQuantity;

    /**
     * 已入库数量
     */
    private Long storageQuantity;

    /**
     * 已入库金额
     */
    private BigDecimal storageAmount;

    /**
     * 已发货数量
     */
    private Long shippedQuantity;

    /**
     * 发货状态
     */
    private String shippedStatus;

    /**
     * 备注
     */
    private String remark;


}
