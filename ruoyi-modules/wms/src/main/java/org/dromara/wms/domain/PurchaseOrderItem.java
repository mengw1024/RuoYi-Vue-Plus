package org.dromara.wms.domain;

import org.dromara.common.tenant.core.TenantEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serial;

/**
 * 采购订单明细对象 purchase_order_item
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("purchase_order_item")
public class PurchaseOrderItem extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 业务时间
     */
    private Date businessTime;

    /**
     * 供应商ID
     */
    private Long supplierManagementId;

    /**
     * 业务员
     */
    private Long salesmanId;

    /**
     * 收货仓库ID
     */
    private Long warehouseId;

    /**
     * 规格编码
     */
    private String skuCode;

    /**
     * 颜色
     */
    private String color;

    /**
     * 重量
     */
    private Long weight;

    /**
     * 体积
     */
    private Long volume;

    /**
     * 长
     */
    private Long length;

    /**
     * 宽
     */
    private Long wide;

    /**
     * 高
     */
    private Long high;

    /**
     * 批发价
     */
    private BigDecimal tradePrice;

    /**
     * 进价
     */
    private BigDecimal purchasePrice;

    /**
     * 备注
     */
    private String remark;


}
