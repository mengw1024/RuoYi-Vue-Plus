package org.dromara.wms.domain;

import org.dromara.common.tenant.core.TenantEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;

import java.io.Serial;

/**
 * 供应商管理对象 supplier
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("supplier")
public class Supplier extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 供应商编码
     */
    private String code;

    /**
     * 供应商名称
     */
    private String name;

    /**
     * 供应商类型 1.普通供应商 2.直供供应商 3.1688供应商
     */
    private String type;

    /**
     * 网站网址
     */
    private String email;

    /**
     * 当前应付款
     */
    private BigDecimal due;

    /**
     * 预付款
     */
    private BigDecimal advance;

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 区
     */
    private String district;

    /**
     * 详细地区
     */
    private String address;

    /**
     * 联系人
     */
    private String linkman;

    /**
     * 固话
     */
    private String landLine;

    /**
     * 手机
     */
    private String phone;

    /**
     * 1688供应商编码
     */
    private String alibabaSupplierCode;

    /**
     * 备注
     */
    private String remark;


}
