package org.dromara.wms.domain.bo;

import org.dromara.wms.domain.BasicsArea;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;

/**
 * 库区业务对象 basics_area
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = BasicsArea.class, reverseConvertGenerate = false)
public class BasicsAreaBo extends BaseEntity {

    /**
     * 主键
     */
    @NotNull(message = "主键不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 库区编码
     */
    @NotBlank(message = "库区编码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String code;

    /**
     * 库区名称
     */
    @NotBlank(message = "库区名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 库区类型 1.捡货区 2.备货区
     */
    @NotNull(message = "库区类型 1.捡货区 2.备货区不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long type;

    /**
     * 仓库id
     */
    @NotNull(message = "仓库id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long warehouseId;

    /**
     * 仓库编码
     */
    @NotBlank(message = "仓库编码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String warehouseCode;

    /**
     * 备注
     */
    private String remark;


}
