package org.dromara.wms.domain.bo;

import org.dromara.wms.domain.BasicsLocation;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;

/**
 * 货位业务对象 basics_location
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = BasicsLocation.class, reverseConvertGenerate = false)
public class BasicsLocationBo extends BaseEntity {

    /**
     * 主键
     */
    @NotNull(message = "主键不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 仓库ID
     */
    @NotNull(message = "仓库ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long warehouseId;

    /**
     * 仓库编码
     */
    @NotBlank(message = "仓库编码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String warehouseCode;

    /**
     * 库区ID
     */
    @NotNull(message = "库区ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long areaId;

    /**
     * 库区编码
     */
    @NotBlank(message = "库区编码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String areaCode;

    /**
     * 货架ID
     */
    @NotNull(message = "货架ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long rackId;

    /**
     * 货架编码
     */
    @NotBlank(message = "货架编码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String rackCode;

    /**
     * 货位编码
     */
    @NotBlank(message = "货位编码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String code;

    /**
     * 货位类型
     */
    @NotNull(message = "货位类型不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long type;

    /**
     * ABC分类
     */
    @NotBlank(message = "ABC分类不能为空", groups = { AddGroup.class, EditGroup.class })
    private String abcType;

    /**
     * 重量上限
     */
    @NotNull(message = "重量上限不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long weight;

    /**
     * 体积上限
     */
    @NotNull(message = "体积上限不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long volume;

    /**
     * 补货触发量
     */
    @NotNull(message = "补货触发量不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long reorderPoint;

    /**
     * 补货上限
     */
    @NotNull(message = "补货上限不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long restockCeiling;

    /**
     * 商品混放
     */
    private String commodityMixing;

    /**
     * 批次混放
     */
    private String batchMixing;

    /**
     * 排序
     */
    @NotNull(message = "排序不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long sort;

    /**
     * 尺寸-长
     */
    @NotNull(message = "尺寸-长不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long sizeLength;

    /**
     * 尺寸-宽
     */
    @NotNull(message = "尺寸-宽不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long sizeWidth;

    /**
     * 尺寸-高
     */
    @NotNull(message = "尺寸-高不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long sizeHigh;

    /**
     * 排位层-排
     */
    @NotNull(message = "排位层-排不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long levelRow;

    /**
     * 排位层-位
     */
    @NotNull(message = "排位层-位不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long levelSlot;

    /**
     * 排位层-层
     */
    @NotNull(message = "排位层-层不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long levelTier;

    /**
     * 备注
     */
    private String remark;


}
