package org.dromara.wms.domain.bo;

import org.dromara.wms.domain.BasicsRack;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;

/**
 * 货架业务对象 basics_rack
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = BasicsRack.class, reverseConvertGenerate = false)
public class BasicsRackBo extends BaseEntity {

    /**
     * 主键
     */
    @NotNull(message = "主键不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 货架编码
     */
    @NotBlank(message = "货架编码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String code;

    /**
     * 货架类型
     */
    @NotNull(message = "货架类型不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long type;

    /**
     * 库区ID
     */
    @NotNull(message = "库区ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long areaId;

    /**
     * 库区编码
     */
    @NotBlank(message = "库区编码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String areaCode;

    /**
     * 仓库ID
     */
    @NotNull(message = "仓库ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long warehouseId;

    /**
     * 仓库编码
     */
    @NotBlank(message = "仓库编码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String warehouseCode;

    /**
     * 备注
     */
    private String remark;


}
