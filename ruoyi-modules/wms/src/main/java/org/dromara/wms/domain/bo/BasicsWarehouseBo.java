package org.dromara.wms.domain.bo;

import org.dromara.wms.domain.BasicsWarehouse;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;

/**
 * 仓库信息业务对象 basics_warehouse
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = BasicsWarehouse.class, reverseConvertGenerate = false)
public class BasicsWarehouseBo extends BaseEntity {

    /**
     * 主键
     */
    @NotNull(message = "主键不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 仓库编码
     */
    @NotBlank(message = "仓库编码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String code;

    /**
     * 仓库名称
     */
    @NotBlank(message = "仓库名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 管理员ID
     */
    @NotBlank(message = "管理员ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private String managerId;

    /**
     * 管理员姓名
     */
    @NotBlank(message = "管理员姓名不能为空", groups = { AddGroup.class, EditGroup.class })
    private String managerName;

    /**
     * 联系地址
     */
    @NotBlank(message = "联系地址不能为空", groups = { AddGroup.class, EditGroup.class })
    private String address;

    /**
     * 备注
     */
    private String remark;


}
