package org.dromara.wms.domain.bo;

import org.dromara.wms.domain.GoodBrands;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;

/**
 * 品牌业务对象 good_brands
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = GoodBrands.class, reverseConvertGenerate = false)
public class GoodBrandsBo extends BaseEntity {

    /**
     * 品牌ID
     */
    @NotNull(message = "品牌ID不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 品牌名
     */
    @NotBlank(message = "品牌名不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 备注
     */
    private String remark;


}
