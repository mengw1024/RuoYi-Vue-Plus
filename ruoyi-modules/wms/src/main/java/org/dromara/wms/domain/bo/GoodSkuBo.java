package org.dromara.wms.domain.bo;

import org.dromara.wms.domain.GoodSku;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;
import java.math.BigDecimal;

/**
 * 商品规格业务对象 good_sku
 *
 * @author Lion Li
 * @date 2024-01-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = GoodSku.class, reverseConvertGenerate = false)
public class GoodSkuBo extends BaseEntity {

    /**
     * id
     */
    @NotNull(message = "id不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 商品ID
     */
    @NotNull(message = "商品ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long goodId;

    /**
     * 规格编码
     */
    private String code;

    /**
     * 商品条码
     */
    private String barCode;

    /**
     * 颜色
     */
    private String color;

    /**
     * 重量
     */
    private Long weight;

    /**
     * 体积
     */
    private Long volume;

    /**
     * 长
     */
    private Long length;

    /**
     * 宽
     */
    private Long wide;

    /**
     * 高
     */
    private Long high;

    /**
     * 售价
     */
    private BigDecimal sellingPrice;

    /**
     * 批发价
     */
    private BigDecimal tradePrice;

    /**
     * 进价
     */
    private BigDecimal purchasePrice;

    /**
     * 备注
     */
    private String remark;


}
