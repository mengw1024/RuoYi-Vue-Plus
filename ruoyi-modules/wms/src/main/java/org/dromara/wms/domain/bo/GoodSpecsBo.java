package org.dromara.wms.domain.bo;

import org.dromara.wms.domain.GoodSpecs;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;
import java.math.BigDecimal;

/**
 * 商品规格业务对象 good_specs
 *
 * @author Lion Li
 * @date 2024-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = GoodSpecs.class, reverseConvertGenerate = false)
public class GoodSpecsBo extends BaseEntity {

    /**
     * ID
     */
    @NotNull(message = "ID不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 规格编码
     */
    @NotBlank(message = "规格编码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String code;

    /**
     * 颜色
     */
    private String color;

    /**
     * 重量
     */
    private Long weight;

    /**
     * 体积
     */
    private Long volume;

    /**
     * 长
     */
    private Long length;

    /**
     * 宽
     */
    private Long wide;

    /**
     * 高
     */
    private Long high;

    /**
     * 批发价
     */
    @NotNull(message = "批发价不能为空", groups = { AddGroup.class, EditGroup.class })
    private BigDecimal tradePrice;

    /**
     * 进价
     */
    private BigDecimal purchasePrice;

    /**
     * 操作
     */
    private String operation;

    /**
     * 备注
     */
    private String remark;


}
