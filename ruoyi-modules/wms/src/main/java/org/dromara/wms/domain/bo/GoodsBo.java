package org.dromara.wms.domain.bo;

import org.dromara.wms.domain.GoodSku;
import org.dromara.wms.domain.Goods;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;

import java.util.List;

/**
 * 商品业务对象 goods
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = Goods.class, reverseConvertGenerate = false)
public class GoodsBo extends BaseEntity {

    /**
     * ID
     */
    @NotNull(message = "ID不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 商品编码
     */
    @NotBlank(message = "商品编码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String code;

    /**
     * 商品名称
     */
    @NotBlank(message = "商品名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 品牌ID
     */
    private Long brandId;

    /**
     * 单位
     */
    private String unit;

    /**
     * 商品标记
     */
    private String sign;

    /**
     * 货号
     */
    @NotBlank(message = "货号不能为空", groups = { AddGroup.class, EditGroup.class })
    private String number;

    /**
     * 备注
     */
    private String remark;

    /*
    * sku
    * */
    private List<GoodSku> goodSkuList;
}
