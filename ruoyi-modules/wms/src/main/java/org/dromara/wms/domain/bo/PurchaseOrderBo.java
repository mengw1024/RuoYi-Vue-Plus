package org.dromara.wms.domain.bo;

import org.dromara.wms.domain.PurchaseOrder;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;
import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 采购订单业务对象 purchase_order
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = PurchaseOrder.class, reverseConvertGenerate = false)
public class PurchaseOrderBo extends BaseEntity {

    /**
     * ID
     */
    @NotNull(message = "ID不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 业务时间
     */
    @NotNull(message = "业务时间不能为空", groups = { AddGroup.class, EditGroup.class })
    private Date businessTime;

    /**
     * 单据编号
     */
    @NotBlank(message = "单据编号不能为空", groups = { AddGroup.class, EditGroup.class })
    private String code;

    /**
     * 供应商ID
     */
    @NotNull(message = "供应商ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long supplierManagementId;

    /**
     * 业务员
     */
    @NotNull(message = "业务员不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long salesmanId;

    /**
     * 收获仓库ID
     */
    @NotNull(message = "收获仓库ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long warehouseId;

    /**
     * 状态
     */
    private String status;

    /**
     * 商品总金额
     */
    @NotNull(message = "商品总金额不能为空", groups = { AddGroup.class, EditGroup.class })
    private BigDecimal goodsTotalMoney;

    /**
     * 采购运费
     */
    @NotNull(message = "采购运费不能为空", groups = { AddGroup.class, EditGroup.class })
    private BigDecimal purchaseFreight;

    /**
     * 其他费用
     */
    @NotNull(message = "其他费用不能为空", groups = { AddGroup.class, EditGroup.class })
    private BigDecimal otherCost;

    /**
     * 预付金额
     */
    @NotNull(message = "预付金额不能为空", groups = { AddGroup.class, EditGroup.class })
    private BigDecimal amountAdvance;

    /**
     * 物流单号
     */
    private String trackNumber;

    /**
     * 总采购数量
     */
    @NotNull(message = "总采购数量不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long totalPurchaseQuantity;

    /**
     * 已入库数量
     */
    @NotNull(message = "已入库数量不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long storageQuantity;

    /**
     * 已入库金额
     */
    @NotNull(message = "已入库金额不能为空", groups = { AddGroup.class, EditGroup.class })
    private BigDecimal storageAmount;

    /**
     * 已发货数量
     */
    @NotNull(message = "已发货数量不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long shippedQuantity;

    /**
     * 发货状态
     */
    private String shippedStatus;

    /**
     * 备注
     */
    private String remark;


}
