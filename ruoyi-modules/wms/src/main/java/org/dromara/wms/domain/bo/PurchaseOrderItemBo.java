package org.dromara.wms.domain.bo;

import org.dromara.wms.domain.PurchaseOrderItem;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;
import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 采购订单明细业务对象 purchase_order_item
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = PurchaseOrderItem.class, reverseConvertGenerate = false)
public class PurchaseOrderItemBo extends BaseEntity {

    /**
     * ID
     */
    @NotNull(message = "ID不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 业务时间
     */
    @NotNull(message = "业务时间不能为空", groups = { AddGroup.class, EditGroup.class })
    private Date businessTime;

    /**
     * 供应商ID
     */
    @NotNull(message = "供应商ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long supplierManagementId;

    /**
     * 业务员
     */
    @NotNull(message = "业务员不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long salesmanId;

    /**
     * 收货仓库ID
     */
    @NotNull(message = "收货仓库ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long warehouseId;

    /**
     * 规格编码
     */
    private String skuCode;

    /**
     * 颜色
     */
    private String color;

    /**
     * 重量
     */
    private Long weight;

    /**
     * 体积
     */
    private Long volume;

    /**
     * 长
     */
    private Long length;

    /**
     * 宽
     */
    private Long wide;

    /**
     * 高
     */
    private Long high;

    /**
     * 批发价
     */
    private BigDecimal tradePrice;

    /**
     * 进价
     */
    private BigDecimal purchasePrice;

    /**
     * 备注
     */
    private String remark;


}
