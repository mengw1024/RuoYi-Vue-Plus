package org.dromara.wms.domain.bo;

import org.dromara.wms.domain.Supplier;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;
import java.math.BigDecimal;

/**
 * 供应商管理业务对象 supplier
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = Supplier.class, reverseConvertGenerate = false)
public class SupplierBo extends BaseEntity {

    /**
     * 主键id
     */
    @NotNull(message = "主键id不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 供应商编码
     */
    private String code;

    /**
     * 供应商名称
     */
    @NotBlank(message = "供应商名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 供应商类型 1.普通供应商 2.直供供应商 3.1688供应商
     */
    private String type;

    /**
     * 网站网址
     */
    private String email;

    /**
     * 当前应付款
     */
    private BigDecimal due;

    /**
     * 预付款
     */
    private BigDecimal advance;

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 区
     */
    private String district;

    /**
     * 详细地区
     */
    private String address;

    /**
     * 联系人
     */
    private String linkman;

    /**
     * 固话
     */
    private String landLine;

    /**
     * 手机
     */
    private String phone;

    /**
     * 1688供应商编码
     */
    private String alibabaSupplierCode;

    /**
     * 备注
     */
    private String remark;


}
