package org.dromara.wms.domain.vo;

import org.dromara.wms.domain.BasicsArea;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 库区视图对象 basics_area
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = BasicsArea.class)
public class BasicsAreaVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ExcelProperty(value = "主键")
    private Long id;

    /**
     * 库区编码
     */
    @ExcelProperty(value = "库区编码")
    private String code;

    /**
     * 库区名称
     */
    @ExcelProperty(value = "库区名称")
    private String name;

    /**
     * 库区类型 1.捡货区 2.备货区
     */
    @ExcelProperty(value = "库区类型 1.捡货区 2.备货区")
    private Long type;

    /**
     * 仓库id
     */
    @ExcelProperty(value = "仓库id")
    private Long warehouseId;

    /**
     * 仓库编码
     */
    @ExcelProperty(value = "仓库编码")
    private String warehouseCode;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


}
