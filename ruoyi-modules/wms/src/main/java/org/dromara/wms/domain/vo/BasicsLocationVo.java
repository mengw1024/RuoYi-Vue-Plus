package org.dromara.wms.domain.vo;

import org.dromara.wms.domain.BasicsLocation;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 货位视图对象 basics_location
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = BasicsLocation.class)
public class BasicsLocationVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ExcelProperty(value = "主键")
    private Long id;

    /**
     * 仓库ID
     */
    @ExcelProperty(value = "仓库ID")
    private Long warehouseId;

    /**
     * 仓库编码
     */
    @ExcelProperty(value = "仓库编码")
    private String warehouseCode;

    /**
     * 库区ID
     */
    @ExcelProperty(value = "库区ID")
    private Long areaId;

    /**
     * 库区编码
     */
    @ExcelProperty(value = "库区编码")
    private String areaCode;

    /**
     * 货架ID
     */
    @ExcelProperty(value = "货架ID")
    private Long rackId;

    /**
     * 货架编码
     */
    @ExcelProperty(value = "货架编码")
    private String rackCode;

    /**
     * 货位编码
     */
    @ExcelProperty(value = "货位编码")
    private String code;

    /**
     * 货位类型
     */
    @ExcelProperty(value = "货位类型")
    private Long type;

    /**
     * ABC分类
     */
    @ExcelProperty(value = "ABC分类")
    private String abcType;

    /**
     * 重量上限
     */
    @ExcelProperty(value = "重量上限")
    private Long weight;

    /**
     * 体积上限
     */
    @ExcelProperty(value = "体积上限")
    private Long volume;

    /**
     * 补货触发量
     */
    @ExcelProperty(value = "补货触发量")
    private Long reorderPoint;

    /**
     * 补货上限
     */
    @ExcelProperty(value = "补货上限")
    private Long restockCeiling;

    /**
     * 商品混放
     */
    @ExcelProperty(value = "商品混放")
    private String commodityMixing;

    /**
     * 批次混放
     */
    @ExcelProperty(value = "批次混放")
    private String batchMixing;

    /**
     * 排序
     */
    @ExcelProperty(value = "排序")
    private Long sort;

    /**
     * 尺寸-长
     */
    @ExcelProperty(value = "尺寸-长")
    private Long sizeLength;

    /**
     * 尺寸-宽
     */
    @ExcelProperty(value = "尺寸-宽")
    private Long sizeWidth;

    /**
     * 尺寸-高
     */
    @ExcelProperty(value = "尺寸-高")
    private Long sizeHigh;

    /**
     * 排位层-排
     */
    @ExcelProperty(value = "排位层-排")
    private Long levelRow;

    /**
     * 排位层-位
     */
    @ExcelProperty(value = "排位层-位")
    private Long levelSlot;

    /**
     * 排位层-层
     */
    @ExcelProperty(value = "排位层-层")
    private Long levelTier;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


}
