package org.dromara.wms.domain.vo;

import org.dromara.wms.domain.BasicsRack;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 货架视图对象 basics_rack
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = BasicsRack.class)
public class BasicsRackVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ExcelProperty(value = "主键")
    private Long id;

    /**
     * 货架编码
     */
    @ExcelProperty(value = "货架编码")
    private String code;

    /**
     * 货架类型
     */
    @ExcelProperty(value = "货架类型")
    private Long type;

    /**
     * 库区ID
     */
    @ExcelProperty(value = "库区ID")
    private Long areaId;

    /**
     * 库区编码
     */
    @ExcelProperty(value = "库区编码")
    private String areaCode;

    /**
     * 仓库ID
     */
    @ExcelProperty(value = "仓库ID")
    private Long warehouseId;

    /**
     * 仓库编码
     */
    @ExcelProperty(value = "仓库编码")
    private String warehouseCode;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


}
