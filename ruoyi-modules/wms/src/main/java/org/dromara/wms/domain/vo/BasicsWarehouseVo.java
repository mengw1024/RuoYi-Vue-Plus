package org.dromara.wms.domain.vo;

import org.dromara.wms.domain.BasicsWarehouse;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 仓库信息视图对象 basics_warehouse
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = BasicsWarehouse.class)
public class BasicsWarehouseVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ExcelProperty(value = "主键")
    private Long id;

    /**
     * 仓库编码
     */
    @ExcelProperty(value = "仓库编码")
    private String code;

    /**
     * 仓库名称
     */
    @ExcelProperty(value = "仓库名称")
    private String name;

    /**
     * 管理员ID
     */
    @ExcelProperty(value = "管理员ID")
    private String managerId;

    /**
     * 管理员姓名
     */
    @ExcelProperty(value = "管理员姓名")
    private String managerName;

    /**
     * 联系地址
     */
    @ExcelProperty(value = "联系地址")
    private String address;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


}
