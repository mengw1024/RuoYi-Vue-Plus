package org.dromara.wms.domain.vo;

import org.dromara.wms.domain.Brands;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 品牌视图对象 brands
 *
 * @author Lion Li
 * @date 2024-01-11
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = Brands.class)
public class BrandsVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @ExcelProperty(value = "ID")
    private Long id;

    /**
     * 品牌名
     */
    @ExcelProperty(value = "品牌名")
    private String name;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


}
