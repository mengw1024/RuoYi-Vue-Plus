package org.dromara.wms.domain.vo;

import java.math.BigDecimal;
import org.dromara.wms.domain.GoodSpecs;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 商品规格视图对象 good_specs
 *
 * @author Lion Li
 * @date 2024-01-11
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = GoodSpecs.class)
public class GoodSpecsVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @ExcelProperty(value = "ID")
    private Long id;

    /**
     * 规格编码
     */
    @ExcelProperty(value = "规格编码")
    private String code;

    /**
     * 颜色
     */
    @ExcelProperty(value = "颜色")
    private String color;

    /**
     * 重量
     */
    @ExcelProperty(value = "重量")
    private Long weight;

    /**
     * 体积
     */
    @ExcelProperty(value = "体积")
    private Long volume;

    /**
     * 长
     */
    @ExcelProperty(value = "长")
    private Long length;

    /**
     * 宽
     */
    @ExcelProperty(value = "宽")
    private Long wide;

    /**
     * 高
     */
    @ExcelProperty(value = "高")
    private Long high;

    /**
     * 批发价
     */
    @ExcelProperty(value = "批发价")
    private BigDecimal tradePrice;

    /**
     * 进价
     */
    @ExcelProperty(value = "进价")
    private BigDecimal purchasePrice;

    /**
     * 操作
     */
    @ExcelProperty(value = "操作")
    private String operation;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


}
