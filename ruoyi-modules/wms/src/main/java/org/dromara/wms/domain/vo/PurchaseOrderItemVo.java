package org.dromara.wms.domain.vo;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.dromara.wms.domain.PurchaseOrderItem;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 采购订单明细视图对象 purchase_order_item
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = PurchaseOrderItem.class)
public class PurchaseOrderItemVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @ExcelProperty(value = "ID")
    private Long id;

    /**
     * 业务时间
     */
    @ExcelProperty(value = "业务时间")
    private Date businessTime;

    /**
     * 供应商ID
     */
    @ExcelProperty(value = "供应商ID")
    private Long supplierManagementId;

    /**
     * 业务员
     */
    @ExcelProperty(value = "业务员")
    private Long salesmanId;

    /**
     * 收货仓库ID
     */
    @ExcelProperty(value = "收货仓库ID")
    private Long warehouseId;

    /**
     * 规格编码
     */
    @ExcelProperty(value = "规格编码")
    private String skuCode;

    /**
     * 颜色
     */
    @ExcelProperty(value = "颜色")
    private String color;

    /**
     * 重量
     */
    @ExcelProperty(value = "重量")
    private Long weight;

    /**
     * 体积
     */
    @ExcelProperty(value = "体积")
    private Long volume;

    /**
     * 长
     */
    @ExcelProperty(value = "长")
    private Long length;

    /**
     * 宽
     */
    @ExcelProperty(value = "宽")
    private Long wide;

    /**
     * 高
     */
    @ExcelProperty(value = "高")
    private Long high;

    /**
     * 批发价
     */
    @ExcelProperty(value = "批发价")
    private BigDecimal tradePrice;

    /**
     * 进价
     */
    @ExcelProperty(value = "进价")
    private BigDecimal purchasePrice;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


}
