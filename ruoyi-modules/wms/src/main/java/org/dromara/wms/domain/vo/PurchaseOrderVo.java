package org.dromara.wms.domain.vo;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.dromara.wms.domain.PurchaseOrder;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 采购订单视图对象 purchase_order
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = PurchaseOrder.class)
public class PurchaseOrderVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @ExcelProperty(value = "ID")
    private Long id;

    /**
     * 业务时间
     */
    @ExcelProperty(value = "业务时间")
    private Date businessTime;

    /**
     * 单据编号
     */
    @ExcelProperty(value = "单据编号")
    private String code;

    /**
     * 供应商ID
     */
    @ExcelProperty(value = "供应商ID")
    private Long supplierManagementId;

    /**
     * 业务员
     */
    @ExcelProperty(value = "业务员")
    private Long salesmanId;

    /**
     * 收获仓库ID
     */
    @ExcelProperty(value = "收获仓库ID")
    private Long warehouseId;

    /**
     * 状态
     */
    @ExcelProperty(value = "状态")
    private String status;

    /**
     * 商品总金额
     */
    @ExcelProperty(value = "商品总金额")
    private BigDecimal goodsTotalMoney;

    /**
     * 采购运费
     */
    @ExcelProperty(value = "采购运费")
    private BigDecimal purchaseFreight;

    /**
     * 其他费用
     */
    @ExcelProperty(value = "其他费用")
    private BigDecimal otherCost;

    /**
     * 预付金额
     */
    @ExcelProperty(value = "预付金额")
    private BigDecimal amountAdvance;

    /**
     * 物流单号
     */
    @ExcelProperty(value = "物流单号")
    private String trackNumber;

    /**
     * 总采购数量
     */
    @ExcelProperty(value = "总采购数量")
    private Long totalPurchaseQuantity;

    /**
     * 已入库数量
     */
    @ExcelProperty(value = "已入库数量")
    private Long storageQuantity;

    /**
     * 已入库金额
     */
    @ExcelProperty(value = "已入库金额")
    private BigDecimal storageAmount;

    /**
     * 已发货数量
     */
    @ExcelProperty(value = "已发货数量")
    private Long shippedQuantity;

    /**
     * 发货状态
     */
    @ExcelProperty(value = "发货状态")
    private String shippedStatus;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


}
