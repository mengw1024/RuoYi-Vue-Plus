package org.dromara.wms.domain.vo;

import java.math.BigDecimal;
import org.dromara.wms.domain.Supplier;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 供应商管理视图对象 supplier
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = Supplier.class)
public class SupplierVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @ExcelProperty(value = "主键id")
    private Long id;

    /**
     * 供应商编码
     */
    @ExcelProperty(value = "供应商编码")
    private String code;

    /**
     * 供应商名称
     */
    @ExcelProperty(value = "供应商名称")
    private String name;

    /**
     * 供应商类型 1.普通供应商 2.直供供应商 3.1688供应商
     */
    @ExcelProperty(value = "供应商类型 1.普通供应商 2.直供供应商 3.1688供应商")
    private String type;

    /**
     * 网站网址
     */
    @ExcelProperty(value = "网站网址")
    private String email;

    /**
     * 当前应付款
     */
    @ExcelProperty(value = "当前应付款")
    private BigDecimal due;

    /**
     * 预付款
     */
    @ExcelProperty(value = "预付款")
    private BigDecimal advance;

    /**
     * 省
     */
    @ExcelProperty(value = "省")
    private String province;

    /**
     * 市
     */
    @ExcelProperty(value = "市")
    private String city;

    /**
     * 区
     */
    @ExcelProperty(value = "区")
    private String district;

    /**
     * 详细地区
     */
    @ExcelProperty(value = "详细地区")
    private String address;

    /**
     * 联系人
     */
    @ExcelProperty(value = "联系人")
    private String linkman;

    /**
     * 固话
     */
    @ExcelProperty(value = "固话")
    private String landLine;

    /**
     * 手机
     */
    @ExcelProperty(value = "手机")
    private String phone;

    /**
     * 1688供应商编码
     */
    @ExcelProperty(value = "1688供应商编码")
    private String alibabaSupplierCode;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


}
