package org.dromara.wms.mapper;

import org.dromara.wms.domain.BasicsLocation;
import org.dromara.wms.domain.vo.BasicsLocationVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 货位Mapper接口
 *
 * @author Lion Li
 * @date 2024-01-12
 */
public interface BasicsLocationMapper extends BaseMapperPlus<BasicsLocation, BasicsLocationVo> {

}
