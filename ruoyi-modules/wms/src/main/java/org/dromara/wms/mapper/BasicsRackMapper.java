package org.dromara.wms.mapper;

import org.dromara.wms.domain.BasicsRack;
import org.dromara.wms.domain.vo.BasicsRackVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 货架Mapper接口
 *
 * @author Lion Li
 * @date 2024-01-12
 */
public interface BasicsRackMapper extends BaseMapperPlus<BasicsRack, BasicsRackVo> {

}
