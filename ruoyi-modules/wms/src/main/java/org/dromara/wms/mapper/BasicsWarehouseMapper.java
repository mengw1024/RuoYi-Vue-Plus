package org.dromara.wms.mapper;

import org.dromara.wms.domain.BasicsWarehouse;
import org.dromara.wms.domain.vo.BasicsWarehouseVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 仓库信息Mapper接口
 *
 * @author Lion Li
 * @date 2024-01-12
 */
public interface BasicsWarehouseMapper extends BaseMapperPlus<BasicsWarehouse, BasicsWarehouseVo> {

}
