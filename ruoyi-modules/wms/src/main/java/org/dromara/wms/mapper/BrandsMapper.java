package org.dromara.wms.mapper;

import org.dromara.wms.domain.Brands;
import org.dromara.wms.domain.vo.BrandsVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 品牌Mapper接口
 *
 * @author Lion Li
 * @date 2024-01-11
 */
public interface BrandsMapper extends BaseMapperPlus<Brands, BrandsVo> {

}
