package org.dromara.wms.mapper;

import org.dromara.wms.domain.GoodBrands;
import org.dromara.wms.domain.vo.GoodBrandsVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 品牌Mapper接口
 *
 * @author Lion Li
 * @date 2024-01-12
 */
public interface GoodBrandsMapper extends BaseMapperPlus<GoodBrands, GoodBrandsVo> {

}
