package org.dromara.wms.mapper;

import org.dromara.wms.domain.GoodSku;
import org.dromara.wms.domain.vo.GoodSkuVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 商品规格Mapper接口
 *
 * @author Lion Li
 * @date 2024-01-15
 */
public interface GoodSkuMapper extends BaseMapperPlus<GoodSku, GoodSkuVo> {

}
