package org.dromara.wms.mapper;

import org.dromara.wms.domain.GoodSpecs;
import org.dromara.wms.domain.vo.GoodSpecsVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 商品规格Mapper接口
 *
 * @author Lion Li
 * @date 2024-01-11
 */
public interface GoodSpecsMapper extends BaseMapperPlus<GoodSpecs, GoodSpecsVo> {

}
