package org.dromara.wms.mapper;

import org.dromara.wms.domain.Goods;
import org.dromara.wms.domain.vo.GoodsVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 商品Mapper接口
 *
 * @author Lion Li
 * @date 2024-01-12
 */
public interface GoodsMapper extends BaseMapperPlus<Goods, GoodsVo> {

}
