package org.dromara.wms.mapper;

import org.dromara.wms.domain.PurchaseOrderItem;
import org.dromara.wms.domain.vo.PurchaseOrderItemVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 采购订单明细Mapper接口
 *
 * @author Lion Li
 * @date 2024-01-12
 */
public interface PurchaseOrderItemMapper extends BaseMapperPlus<PurchaseOrderItem, PurchaseOrderItemVo> {

}
