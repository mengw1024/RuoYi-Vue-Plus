package org.dromara.wms.mapper;

import org.dromara.wms.domain.PurchaseOrder;
import org.dromara.wms.domain.vo.PurchaseOrderVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 采购订单Mapper接口
 *
 * @author Lion Li
 * @date 2024-01-12
 */
public interface PurchaseOrderMapper extends BaseMapperPlus<PurchaseOrder, PurchaseOrderVo> {

}
