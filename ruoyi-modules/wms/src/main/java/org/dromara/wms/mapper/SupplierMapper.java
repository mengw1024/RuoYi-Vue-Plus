package org.dromara.wms.mapper;

import org.dromara.wms.domain.Supplier;
import org.dromara.wms.domain.vo.SupplierVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 供应商管理Mapper接口
 *
 * @author Lion Li
 * @date 2024-01-12
 */
public interface SupplierMapper extends BaseMapperPlus<Supplier, SupplierVo> {

}
