package org.dromara.wms.service;

import org.dromara.wms.domain.BasicsArea;
import org.dromara.wms.domain.vo.BasicsAreaVo;
import org.dromara.wms.domain.bo.BasicsAreaBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 库区Service接口
 *
 * @author Lion Li
 * @date 2024-01-12
 */
public interface IBasicsAreaService {

    /**
     * 查询库区
     */
    BasicsAreaVo queryById(Long id);

    /**
     * 查询库区列表
     */
    TableDataInfo<BasicsAreaVo> queryPageList(BasicsAreaBo bo, PageQuery pageQuery);

    /**
     * 查询库区列表
     */
    List<BasicsAreaVo> queryList(BasicsAreaBo bo);

    /**
     * 新增库区
     */
    Boolean insertByBo(BasicsAreaBo bo);

    /**
     * 修改库区
     */
    Boolean updateByBo(BasicsAreaBo bo);

    /**
     * 校验并批量删除库区信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
