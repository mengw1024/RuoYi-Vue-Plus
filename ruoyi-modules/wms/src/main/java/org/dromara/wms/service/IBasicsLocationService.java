package org.dromara.wms.service;

import org.dromara.wms.domain.BasicsLocation;
import org.dromara.wms.domain.vo.BasicsLocationVo;
import org.dromara.wms.domain.bo.BasicsLocationBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 货位Service接口
 *
 * @author Lion Li
 * @date 2024-01-12
 */
public interface IBasicsLocationService {

    /**
     * 查询货位
     */
    BasicsLocationVo queryById(Long id);

    /**
     * 查询货位列表
     */
    TableDataInfo<BasicsLocationVo> queryPageList(BasicsLocationBo bo, PageQuery pageQuery);

    /**
     * 查询货位列表
     */
    List<BasicsLocationVo> queryList(BasicsLocationBo bo);

    /**
     * 新增货位
     */
    Boolean insertByBo(BasicsLocationBo bo);

    /**
     * 修改货位
     */
    Boolean updateByBo(BasicsLocationBo bo);

    /**
     * 校验并批量删除货位信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
