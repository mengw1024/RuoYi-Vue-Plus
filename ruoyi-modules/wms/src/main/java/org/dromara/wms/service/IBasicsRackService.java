package org.dromara.wms.service;

import org.dromara.wms.domain.BasicsRack;
import org.dromara.wms.domain.vo.BasicsRackVo;
import org.dromara.wms.domain.bo.BasicsRackBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 货架Service接口
 *
 * @author Lion Li
 * @date 2024-01-12
 */
public interface IBasicsRackService {

    /**
     * 查询货架
     */
    BasicsRackVo queryById(Long id);

    /**
     * 查询货架列表
     */
    TableDataInfo<BasicsRackVo> queryPageList(BasicsRackBo bo, PageQuery pageQuery);

    /**
     * 查询货架列表
     */
    List<BasicsRackVo> queryList(BasicsRackBo bo);

    /**
     * 新增货架
     */
    Boolean insertByBo(BasicsRackBo bo);

    /**
     * 修改货架
     */
    Boolean updateByBo(BasicsRackBo bo);

    /**
     * 校验并批量删除货架信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
