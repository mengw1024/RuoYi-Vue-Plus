package org.dromara.wms.service;

import org.dromara.wms.domain.BasicsWarehouse;
import org.dromara.wms.domain.vo.BasicsWarehouseVo;
import org.dromara.wms.domain.bo.BasicsWarehouseBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 仓库信息Service接口
 *
 * @author Lion Li
 * @date 2024-01-12
 */
public interface IBasicsWarehouseService {

    /**
     * 查询仓库信息
     */
    BasicsWarehouseVo queryById(Long id);

    /**
     * 查询仓库信息列表
     */
    TableDataInfo<BasicsWarehouseVo> queryPageList(BasicsWarehouseBo bo, PageQuery pageQuery);

    /**
     * 查询仓库信息列表
     */
    List<BasicsWarehouseVo> queryList(BasicsWarehouseBo bo);

    /**
     * 新增仓库信息
     */
    Boolean insertByBo(BasicsWarehouseBo bo);

    /**
     * 修改仓库信息
     */
    Boolean updateByBo(BasicsWarehouseBo bo);

    /**
     * 校验并批量删除仓库信息信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
