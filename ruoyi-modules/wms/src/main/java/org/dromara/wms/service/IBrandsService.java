package org.dromara.wms.service;

import org.dromara.wms.domain.Brands;
import org.dromara.wms.domain.vo.BrandsVo;
import org.dromara.wms.domain.bo.BrandsBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 品牌Service接口
 *
 * @author Lion Li
 * @date 2024-01-11
 */
public interface IBrandsService {

    /**
     * 查询品牌
     */
    BrandsVo queryById(Long id);

    /**
     * 查询品牌列表
     */
    TableDataInfo<BrandsVo> queryPageList(BrandsBo bo, PageQuery pageQuery);

    /**
     * 查询品牌列表
     */
    List<BrandsVo> queryList(BrandsBo bo);

    /**
     * 新增品牌
     */
    Boolean insertByBo(BrandsBo bo);

    /**
     * 修改品牌
     */
    Boolean updateByBo(BrandsBo bo);

    /**
     * 校验并批量删除品牌信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
