package org.dromara.wms.service;

import org.dromara.wms.domain.GoodBrands;
import org.dromara.wms.domain.vo.GoodBrandsVo;
import org.dromara.wms.domain.bo.GoodBrandsBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 品牌Service接口
 *
 * @author Lion Li
 * @date 2024-01-12
 */
public interface IGoodBrandsService {

    /**
     * 查询品牌
     */
    GoodBrandsVo queryById(Long id);

    /**
     * 查询品牌列表
     */
    TableDataInfo<GoodBrandsVo> queryPageList(GoodBrandsBo bo, PageQuery pageQuery);

    /**
     * 查询品牌列表
     */
    List<GoodBrandsVo> queryList(GoodBrandsBo bo);

    /**
     * 新增品牌
     */
    Boolean insertByBo(GoodBrandsBo bo);

    /**
     * 修改品牌
     */
    Boolean updateByBo(GoodBrandsBo bo);

    /**
     * 校验并批量删除品牌信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
