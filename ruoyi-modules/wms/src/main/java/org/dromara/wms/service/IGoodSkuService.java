package org.dromara.wms.service;

import org.dromara.wms.domain.GoodSku;
import org.dromara.wms.domain.vo.GoodSkuVo;
import org.dromara.wms.domain.bo.GoodSkuBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 商品规格Service接口
 *
 * @author Lion Li
 * @date 2024-01-15
 */
public interface IGoodSkuService {

    /**
     * 查询商品规格
     */
    GoodSkuVo queryById(Long id);

    /**
     * 查询商品规格列表
     */
    TableDataInfo<GoodSkuVo> queryPageList(GoodSkuBo bo, PageQuery pageQuery);

    /**
     * 查询商品规格列表
     */
    List<GoodSkuVo> queryList(GoodSkuBo bo);

    /**
     * 新增商品规格
     */
    Boolean insertByBo(GoodSkuBo bo);

    /**
     * 修改商品规格
     */
    Boolean updateByBo(GoodSkuBo bo);

    /**
     * 校验并批量删除商品规格信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    void save(List<GoodSku> goodSkuList);
}
