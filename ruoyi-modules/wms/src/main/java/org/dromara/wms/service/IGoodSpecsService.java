package org.dromara.wms.service;

import org.dromara.wms.domain.GoodSpecs;
import org.dromara.wms.domain.vo.GoodSpecsVo;
import org.dromara.wms.domain.bo.GoodSpecsBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 商品规格Service接口
 *
 * @author Lion Li
 * @date 2024-01-11
 */
public interface IGoodSpecsService {

    /**
     * 查询商品规格
     */
    GoodSpecsVo queryById(Long id);

    /**
     * 查询商品规格列表
     */
    TableDataInfo<GoodSpecsVo> queryPageList(GoodSpecsBo bo, PageQuery pageQuery);

    /**
     * 查询商品规格列表
     */
    List<GoodSpecsVo> queryList(GoodSpecsBo bo);

    /**
     * 新增商品规格
     */
    Boolean insertByBo(GoodSpecsBo bo);

    /**
     * 修改商品规格
     */
    Boolean updateByBo(GoodSpecsBo bo);

    /**
     * 校验并批量删除商品规格信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
