package org.dromara.wms.service;

import org.dromara.wms.domain.Goods;
import org.dromara.wms.domain.vo.GoodsVo;
import org.dromara.wms.domain.bo.GoodsBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 商品Service接口
 *
 * @author Lion Li
 * @date 2024-01-12
 */
public interface IGoodsService {

    /**
     * 查询商品
     */
    GoodsVo queryById(Long id);

    /**
     * 查询商品列表
     */
    TableDataInfo<GoodsVo> queryPageList(GoodsBo bo, PageQuery pageQuery);

    /**
     * 查询商品列表
     */
    List<GoodsVo> queryList(GoodsBo bo);

    /**
     * 新增商品
     */
    Boolean insertByBo(GoodsBo bo);

    /**
     * 修改商品
     */
    Boolean updateByBo(GoodsBo bo);

    /**
     * 校验并批量删除商品信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
