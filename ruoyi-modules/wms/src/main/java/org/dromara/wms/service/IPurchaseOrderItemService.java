package org.dromara.wms.service;

import org.dromara.wms.domain.PurchaseOrderItem;
import org.dromara.wms.domain.vo.PurchaseOrderItemVo;
import org.dromara.wms.domain.bo.PurchaseOrderItemBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 采购订单明细Service接口
 *
 * @author Lion Li
 * @date 2024-01-12
 */
public interface IPurchaseOrderItemService {

    /**
     * 查询采购订单明细
     */
    PurchaseOrderItemVo queryById(Long id);

    /**
     * 查询采购订单明细列表
     */
    TableDataInfo<PurchaseOrderItemVo> queryPageList(PurchaseOrderItemBo bo, PageQuery pageQuery);

    /**
     * 查询采购订单明细列表
     */
    List<PurchaseOrderItemVo> queryList(PurchaseOrderItemBo bo);

    /**
     * 新增采购订单明细
     */
    Boolean insertByBo(PurchaseOrderItemBo bo);

    /**
     * 修改采购订单明细
     */
    Boolean updateByBo(PurchaseOrderItemBo bo);

    /**
     * 校验并批量删除采购订单明细信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
