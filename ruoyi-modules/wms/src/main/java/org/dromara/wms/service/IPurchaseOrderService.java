package org.dromara.wms.service;

import org.dromara.wms.domain.PurchaseOrder;
import org.dromara.wms.domain.vo.PurchaseOrderVo;
import org.dromara.wms.domain.bo.PurchaseOrderBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 采购订单Service接口
 *
 * @author Lion Li
 * @date 2024-01-12
 */
public interface IPurchaseOrderService {

    /**
     * 查询采购订单
     */
    PurchaseOrderVo queryById(Long id);

    /**
     * 查询采购订单列表
     */
    TableDataInfo<PurchaseOrderVo> queryPageList(PurchaseOrderBo bo, PageQuery pageQuery);

    /**
     * 查询采购订单列表
     */
    List<PurchaseOrderVo> queryList(PurchaseOrderBo bo);

    /**
     * 新增采购订单
     */
    Boolean insertByBo(PurchaseOrderBo bo);

    /**
     * 修改采购订单
     */
    Boolean updateByBo(PurchaseOrderBo bo);

    /**
     * 校验并批量删除采购订单信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
