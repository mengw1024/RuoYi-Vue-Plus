package org.dromara.wms.service;

import org.dromara.wms.domain.Supplier;
import org.dromara.wms.domain.vo.SupplierVo;
import org.dromara.wms.domain.bo.SupplierBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 供应商管理Service接口
 *
 * @author Lion Li
 * @date 2024-01-12
 */
public interface ISupplierService {

    /**
     * 查询供应商管理
     */
    SupplierVo queryById(Long id);

    /**
     * 查询供应商管理列表
     */
    TableDataInfo<SupplierVo> queryPageList(SupplierBo bo, PageQuery pageQuery);

    /**
     * 查询供应商管理列表
     */
    List<SupplierVo> queryList(SupplierBo bo);

    /**
     * 新增供应商管理
     */
    Boolean insertByBo(SupplierBo bo);

    /**
     * 修改供应商管理
     */
    Boolean updateByBo(SupplierBo bo);

    /**
     * 校验并批量删除供应商管理信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
