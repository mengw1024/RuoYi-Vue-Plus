package org.dromara.wms.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.dromara.wms.domain.bo.BasicsAreaBo;
import org.dromara.wms.domain.vo.BasicsAreaVo;
import org.dromara.wms.domain.BasicsArea;
import org.dromara.wms.mapper.BasicsAreaMapper;
import org.dromara.wms.service.IBasicsAreaService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 库区Service业务层处理
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@RequiredArgsConstructor
@Service
public class BasicsAreaServiceImpl implements IBasicsAreaService {

    private final BasicsAreaMapper baseMapper;

    /**
     * 查询库区
     */
    @Override
    public BasicsAreaVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询库区列表
     */
    @Override
    public TableDataInfo<BasicsAreaVo> queryPageList(BasicsAreaBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<BasicsArea> lqw = buildQueryWrapper(bo);
        Page<BasicsAreaVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询库区列表
     */
    @Override
    public List<BasicsAreaVo> queryList(BasicsAreaBo bo) {
        LambdaQueryWrapper<BasicsArea> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<BasicsArea> buildQueryWrapper(BasicsAreaBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<BasicsArea> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getCode()), BasicsArea::getCode, bo.getCode());
        lqw.like(StringUtils.isNotBlank(bo.getName()), BasicsArea::getName, bo.getName());
        lqw.eq(bo.getType() != null, BasicsArea::getType, bo.getType());
        lqw.eq(bo.getWarehouseId() != null, BasicsArea::getWarehouseId, bo.getWarehouseId());
        lqw.eq(StringUtils.isNotBlank(bo.getWarehouseCode()), BasicsArea::getWarehouseCode, bo.getWarehouseCode());
        return lqw;
    }

    /**
     * 新增库区
     */
    @Override
    public Boolean insertByBo(BasicsAreaBo bo) {
        BasicsArea add = MapstructUtils.convert(bo, BasicsArea.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改库区
     */
    @Override
    public Boolean updateByBo(BasicsAreaBo bo) {
        BasicsArea update = MapstructUtils.convert(bo, BasicsArea.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(BasicsArea entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除库区
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
