package org.dromara.wms.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.dromara.wms.domain.bo.BasicsLocationBo;
import org.dromara.wms.domain.vo.BasicsLocationVo;
import org.dromara.wms.domain.BasicsLocation;
import org.dromara.wms.mapper.BasicsLocationMapper;
import org.dromara.wms.service.IBasicsLocationService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 货位Service业务层处理
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@RequiredArgsConstructor
@Service
public class BasicsLocationServiceImpl implements IBasicsLocationService {

    private final BasicsLocationMapper baseMapper;

    /**
     * 查询货位
     */
    @Override
    public BasicsLocationVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询货位列表
     */
    @Override
    public TableDataInfo<BasicsLocationVo> queryPageList(BasicsLocationBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<BasicsLocation> lqw = buildQueryWrapper(bo);
        Page<BasicsLocationVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询货位列表
     */
    @Override
    public List<BasicsLocationVo> queryList(BasicsLocationBo bo) {
        LambdaQueryWrapper<BasicsLocation> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<BasicsLocation> buildQueryWrapper(BasicsLocationBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<BasicsLocation> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getWarehouseId() != null, BasicsLocation::getWarehouseId, bo.getWarehouseId());
        lqw.eq(StringUtils.isNotBlank(bo.getWarehouseCode()), BasicsLocation::getWarehouseCode, bo.getWarehouseCode());
        lqw.eq(bo.getAreaId() != null, BasicsLocation::getAreaId, bo.getAreaId());
        lqw.eq(StringUtils.isNotBlank(bo.getAreaCode()), BasicsLocation::getAreaCode, bo.getAreaCode());
        lqw.eq(bo.getRackId() != null, BasicsLocation::getRackId, bo.getRackId());
        lqw.eq(StringUtils.isNotBlank(bo.getRackCode()), BasicsLocation::getRackCode, bo.getRackCode());
        lqw.eq(StringUtils.isNotBlank(bo.getCode()), BasicsLocation::getCode, bo.getCode());
        lqw.eq(bo.getType() != null, BasicsLocation::getType, bo.getType());
        lqw.eq(StringUtils.isNotBlank(bo.getAbcType()), BasicsLocation::getAbcType, bo.getAbcType());
        lqw.eq(bo.getWeight() != null, BasicsLocation::getWeight, bo.getWeight());
        lqw.eq(bo.getVolume() != null, BasicsLocation::getVolume, bo.getVolume());
        lqw.eq(bo.getReorderPoint() != null, BasicsLocation::getReorderPoint, bo.getReorderPoint());
        lqw.eq(bo.getRestockCeiling() != null, BasicsLocation::getRestockCeiling, bo.getRestockCeiling());
        lqw.eq(StringUtils.isNotBlank(bo.getCommodityMixing()), BasicsLocation::getCommodityMixing, bo.getCommodityMixing());
        lqw.eq(StringUtils.isNotBlank(bo.getBatchMixing()), BasicsLocation::getBatchMixing, bo.getBatchMixing());
        lqw.eq(bo.getSort() != null, BasicsLocation::getSort, bo.getSort());
        lqw.eq(bo.getSizeLength() != null, BasicsLocation::getSizeLength, bo.getSizeLength());
        lqw.eq(bo.getSizeWidth() != null, BasicsLocation::getSizeWidth, bo.getSizeWidth());
        lqw.eq(bo.getSizeHigh() != null, BasicsLocation::getSizeHigh, bo.getSizeHigh());
        lqw.eq(bo.getLevelRow() != null, BasicsLocation::getLevelRow, bo.getLevelRow());
        lqw.eq(bo.getLevelSlot() != null, BasicsLocation::getLevelSlot, bo.getLevelSlot());
        lqw.eq(bo.getLevelTier() != null, BasicsLocation::getLevelTier, bo.getLevelTier());
        return lqw;
    }

    /**
     * 新增货位
     */
    @Override
    public Boolean insertByBo(BasicsLocationBo bo) {
        BasicsLocation add = MapstructUtils.convert(bo, BasicsLocation.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改货位
     */
    @Override
    public Boolean updateByBo(BasicsLocationBo bo) {
        BasicsLocation update = MapstructUtils.convert(bo, BasicsLocation.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(BasicsLocation entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除货位
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
