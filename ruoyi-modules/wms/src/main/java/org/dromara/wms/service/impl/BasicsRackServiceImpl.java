package org.dromara.wms.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.dromara.wms.domain.bo.BasicsRackBo;
import org.dromara.wms.domain.vo.BasicsRackVo;
import org.dromara.wms.domain.BasicsRack;
import org.dromara.wms.mapper.BasicsRackMapper;
import org.dromara.wms.service.IBasicsRackService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 货架Service业务层处理
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@RequiredArgsConstructor
@Service
public class BasicsRackServiceImpl implements IBasicsRackService {

    private final BasicsRackMapper baseMapper;

    /**
     * 查询货架
     */
    @Override
    public BasicsRackVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询货架列表
     */
    @Override
    public TableDataInfo<BasicsRackVo> queryPageList(BasicsRackBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<BasicsRack> lqw = buildQueryWrapper(bo);
        Page<BasicsRackVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询货架列表
     */
    @Override
    public List<BasicsRackVo> queryList(BasicsRackBo bo) {
        LambdaQueryWrapper<BasicsRack> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<BasicsRack> buildQueryWrapper(BasicsRackBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<BasicsRack> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getCode()), BasicsRack::getCode, bo.getCode());
        lqw.eq(bo.getType() != null, BasicsRack::getType, bo.getType());
        lqw.eq(bo.getAreaId() != null, BasicsRack::getAreaId, bo.getAreaId());
        lqw.eq(StringUtils.isNotBlank(bo.getAreaCode()), BasicsRack::getAreaCode, bo.getAreaCode());
        lqw.eq(bo.getWarehouseId() != null, BasicsRack::getWarehouseId, bo.getWarehouseId());
        lqw.eq(StringUtils.isNotBlank(bo.getWarehouseCode()), BasicsRack::getWarehouseCode, bo.getWarehouseCode());
        return lqw;
    }

    /**
     * 新增货架
     */
    @Override
    public Boolean insertByBo(BasicsRackBo bo) {
        BasicsRack add = MapstructUtils.convert(bo, BasicsRack.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改货架
     */
    @Override
    public Boolean updateByBo(BasicsRackBo bo) {
        BasicsRack update = MapstructUtils.convert(bo, BasicsRack.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(BasicsRack entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除货架
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
