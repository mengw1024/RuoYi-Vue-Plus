package org.dromara.wms.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.dromara.wms.domain.bo.BasicsWarehouseBo;
import org.dromara.wms.domain.vo.BasicsWarehouseVo;
import org.dromara.wms.domain.BasicsWarehouse;
import org.dromara.wms.mapper.BasicsWarehouseMapper;
import org.dromara.wms.service.IBasicsWarehouseService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 仓库信息Service业务层处理
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@RequiredArgsConstructor
@Service
public class BasicsWarehouseServiceImpl implements IBasicsWarehouseService {

    private final BasicsWarehouseMapper baseMapper;

    /**
     * 查询仓库信息
     */
    @Override
    public BasicsWarehouseVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询仓库信息列表
     */
    @Override
    public TableDataInfo<BasicsWarehouseVo> queryPageList(BasicsWarehouseBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<BasicsWarehouse> lqw = buildQueryWrapper(bo);
        Page<BasicsWarehouseVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询仓库信息列表
     */
    @Override
    public List<BasicsWarehouseVo> queryList(BasicsWarehouseBo bo) {
        LambdaQueryWrapper<BasicsWarehouse> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<BasicsWarehouse> buildQueryWrapper(BasicsWarehouseBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<BasicsWarehouse> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getCode()), BasicsWarehouse::getCode, bo.getCode());
        lqw.like(StringUtils.isNotBlank(bo.getName()), BasicsWarehouse::getName, bo.getName());
        lqw.eq(StringUtils.isNotBlank(bo.getManagerId()), BasicsWarehouse::getManagerId, bo.getManagerId());
        lqw.like(StringUtils.isNotBlank(bo.getManagerName()), BasicsWarehouse::getManagerName, bo.getManagerName());
        lqw.eq(StringUtils.isNotBlank(bo.getAddress()), BasicsWarehouse::getAddress, bo.getAddress());
        return lqw;
    }

    /**
     * 新增仓库信息
     */
    @Override
    public Boolean insertByBo(BasicsWarehouseBo bo) {
        BasicsWarehouse add = MapstructUtils.convert(bo, BasicsWarehouse.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改仓库信息
     */
    @Override
    public Boolean updateByBo(BasicsWarehouseBo bo) {
        BasicsWarehouse update = MapstructUtils.convert(bo, BasicsWarehouse.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(BasicsWarehouse entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除仓库信息
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
