package org.dromara.wms.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.dromara.wms.domain.bo.BrandsBo;
import org.dromara.wms.domain.vo.BrandsVo;
import org.dromara.wms.domain.Brands;
import org.dromara.wms.mapper.BrandsMapper;
import org.dromara.wms.service.IBrandsService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 品牌Service业务层处理
 *
 * @author Lion Li
 * @date 2024-01-11
 */
@RequiredArgsConstructor
@Service
public class BrandsServiceImpl implements IBrandsService {

    private final BrandsMapper baseMapper;

    /**
     * 查询品牌
     */
    @Override
    public BrandsVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询品牌列表
     */
    @Override
    public TableDataInfo<BrandsVo> queryPageList(BrandsBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<Brands> lqw = buildQueryWrapper(bo);
        Page<BrandsVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询品牌列表
     */
    @Override
    public List<BrandsVo> queryList(BrandsBo bo) {
        LambdaQueryWrapper<Brands> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<Brands> buildQueryWrapper(BrandsBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<Brands> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getName()), Brands::getName, bo.getName());
        return lqw;
    }

    /**
     * 新增品牌
     */
    @Override
    public Boolean insertByBo(BrandsBo bo) {
        Brands add = MapstructUtils.convert(bo, Brands.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改品牌
     */
    @Override
    public Boolean updateByBo(BrandsBo bo) {
        Brands update = MapstructUtils.convert(bo, Brands.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(Brands entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除品牌
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
