package org.dromara.wms.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.dromara.wms.domain.bo.GoodBrandsBo;
import org.dromara.wms.domain.vo.GoodBrandsVo;
import org.dromara.wms.domain.GoodBrands;
import org.dromara.wms.mapper.GoodBrandsMapper;
import org.dromara.wms.service.IGoodBrandsService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 品牌Service业务层处理
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@RequiredArgsConstructor
@Service
public class GoodBrandsServiceImpl implements IGoodBrandsService {

    private final GoodBrandsMapper baseMapper;

    /**
     * 查询品牌
     */
    @Override
    public GoodBrandsVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询品牌列表
     */
    @Override
    public TableDataInfo<GoodBrandsVo> queryPageList(GoodBrandsBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<GoodBrands> lqw = buildQueryWrapper(bo);
        Page<GoodBrandsVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询品牌列表
     */
    @Override
    public List<GoodBrandsVo> queryList(GoodBrandsBo bo) {
        LambdaQueryWrapper<GoodBrands> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<GoodBrands> buildQueryWrapper(GoodBrandsBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<GoodBrands> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getName()), GoodBrands::getName, bo.getName());
        return lqw;
    }

    /**
     * 新增品牌
     */
    @Override
    public Boolean insertByBo(GoodBrandsBo bo) {
        GoodBrands add = MapstructUtils.convert(bo, GoodBrands.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改品牌
     */
    @Override
    public Boolean updateByBo(GoodBrandsBo bo) {
        GoodBrands update = MapstructUtils.convert(bo, GoodBrands.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(GoodBrands entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除品牌
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
