package org.dromara.wms.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.dromara.wms.domain.bo.GoodSkuBo;
import org.dromara.wms.domain.vo.GoodSkuVo;
import org.dromara.wms.domain.GoodSku;
import org.dromara.wms.mapper.GoodSkuMapper;
import org.dromara.wms.service.IGoodSkuService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 商品规格Service业务层处理
 *
 * @author Lion Li
 * @date 2024-01-15
 */
@RequiredArgsConstructor
@Service
public class GoodSkuServiceImpl implements IGoodSkuService {

    private final GoodSkuMapper baseMapper;

    /**
     * 查询商品规格
     */
    @Override
    public GoodSkuVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询商品规格列表
     */
    @Override
    public TableDataInfo<GoodSkuVo> queryPageList(GoodSkuBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<GoodSku> lqw = buildQueryWrapper(bo);
        Page<GoodSkuVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询商品规格列表
     */
    @Override
    public List<GoodSkuVo> queryList(GoodSkuBo bo) {
        LambdaQueryWrapper<GoodSku> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<GoodSku> buildQueryWrapper(GoodSkuBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<GoodSku> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getGoodId() != null, GoodSku::getGoodId, bo.getGoodId());
        lqw.eq(StringUtils.isNotBlank(bo.getCode()), GoodSku::getCode, bo.getCode());
        lqw.eq(StringUtils.isNotBlank(bo.getBarCode()), GoodSku::getBarCode, bo.getBarCode());
        lqw.eq(StringUtils.isNotBlank(bo.getColor()), GoodSku::getColor, bo.getColor());
        lqw.eq(bo.getWeight() != null, GoodSku::getWeight, bo.getWeight());
        lqw.eq(bo.getVolume() != null, GoodSku::getVolume, bo.getVolume());
        lqw.eq(bo.getLength() != null, GoodSku::getLength, bo.getLength());
        lqw.eq(bo.getWide() != null, GoodSku::getWide, bo.getWide());
        lqw.eq(bo.getHigh() != null, GoodSku::getHigh, bo.getHigh());
        lqw.eq(bo.getSellingPrice() != null, GoodSku::getSellingPrice, bo.getSellingPrice());
        lqw.eq(bo.getTradePrice() != null, GoodSku::getTradePrice, bo.getTradePrice());
        lqw.eq(bo.getPurchasePrice() != null, GoodSku::getPurchasePrice, bo.getPurchasePrice());
        return lqw;
    }

    /**
     * 新增商品规格
     */
    @Override
    public Boolean insertByBo(GoodSkuBo bo) {
        GoodSku add = MapstructUtils.convert(bo, GoodSku.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改商品规格
     */
    @Override
    public Boolean updateByBo(GoodSkuBo bo) {
        GoodSku update = MapstructUtils.convert(bo, GoodSku.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(GoodSku entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除商品规格
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public void save(List<GoodSku> goodSkuList) {
        for (GoodSku goodSku : goodSkuList) {
            baseMapper.insert(goodSku);
        }
    }
}
