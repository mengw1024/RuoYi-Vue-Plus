package org.dromara.wms.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.dromara.wms.domain.bo.GoodSpecsBo;
import org.dromara.wms.domain.vo.GoodSpecsVo;
import org.dromara.wms.domain.GoodSpecs;
import org.dromara.wms.mapper.GoodSpecsMapper;
import org.dromara.wms.service.IGoodSpecsService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 商品规格Service业务层处理
 *
 * @author Lion Li
 * @date 2024-01-11
 */
@RequiredArgsConstructor
@Service
public class GoodSpecsServiceImpl implements IGoodSpecsService {

    private final GoodSpecsMapper baseMapper;

    /**
     * 查询商品规格
     */
    @Override
    public GoodSpecsVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询商品规格列表
     */
    @Override
    public TableDataInfo<GoodSpecsVo> queryPageList(GoodSpecsBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<GoodSpecs> lqw = buildQueryWrapper(bo);
        Page<GoodSpecsVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询商品规格列表
     */
    @Override
    public List<GoodSpecsVo> queryList(GoodSpecsBo bo) {
        LambdaQueryWrapper<GoodSpecs> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<GoodSpecs> buildQueryWrapper(GoodSpecsBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<GoodSpecs> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getCode()), GoodSpecs::getCode, bo.getCode());
        lqw.eq(StringUtils.isNotBlank(bo.getColor()), GoodSpecs::getColor, bo.getColor());
        lqw.eq(bo.getWeight() != null, GoodSpecs::getWeight, bo.getWeight());
        lqw.eq(bo.getVolume() != null, GoodSpecs::getVolume, bo.getVolume());
        lqw.eq(bo.getLength() != null, GoodSpecs::getLength, bo.getLength());
        lqw.eq(bo.getWide() != null, GoodSpecs::getWide, bo.getWide());
        lqw.eq(bo.getHigh() != null, GoodSpecs::getHigh, bo.getHigh());
        lqw.eq(bo.getTradePrice() != null, GoodSpecs::getTradePrice, bo.getTradePrice());
        lqw.eq(bo.getPurchasePrice() != null, GoodSpecs::getPurchasePrice, bo.getPurchasePrice());
        lqw.eq(StringUtils.isNotBlank(bo.getOperation()), GoodSpecs::getOperation, bo.getOperation());
        return lqw;
    }

    /**
     * 新增商品规格
     */
    @Override
    public Boolean insertByBo(GoodSpecsBo bo) {
        GoodSpecs add = MapstructUtils.convert(bo, GoodSpecs.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改商品规格
     */
    @Override
    public Boolean updateByBo(GoodSpecsBo bo) {
        GoodSpecs update = MapstructUtils.convert(bo, GoodSpecs.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(GoodSpecs entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除商品规格
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
