package org.dromara.wms.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.dromara.wms.domain.GoodSku;
import org.springframework.stereotype.Service;
import org.dromara.wms.domain.bo.GoodsBo;
import org.dromara.wms.domain.vo.GoodsVo;
import org.dromara.wms.domain.Goods;
import org.dromara.wms.mapper.GoodsMapper;
import org.dromara.wms.service.IGoodsService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 商品Service业务层处理
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@RequiredArgsConstructor
@Service
public class GoodsServiceImpl implements IGoodsService {

    private final GoodsMapper baseMapper;
    private GoodSkuServiceImpl goodSkuService;

    /**
     * 查询商品
     */
    @Override
    public GoodsVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询商品列表
     */
    @Override
    public TableDataInfo<GoodsVo> queryPageList(GoodsBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<Goods> lqw = buildQueryWrapper(bo);
        Page<GoodsVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询商品列表
     */
    @Override
    public List<GoodsVo> queryList(GoodsBo bo) {
        LambdaQueryWrapper<Goods> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<Goods> buildQueryWrapper(GoodsBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<Goods> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getCode()), Goods::getCode, bo.getCode());
        lqw.like(StringUtils.isNotBlank(bo.getName()), Goods::getName, bo.getName());
        lqw.eq(bo.getBrandId() != null, Goods::getBrandId, bo.getBrandId());
        lqw.eq(StringUtils.isNotBlank(bo.getUnit()), Goods::getUnit, bo.getUnit());
        lqw.eq(StringUtils.isNotBlank(bo.getSign()), Goods::getSign, bo.getSign());
        lqw.eq(StringUtils.isNotBlank(bo.getNumber()), Goods::getNumber, bo.getNumber());
        return lqw;
    }

    /**
     * 新增商品
     */
    @Override
    public Boolean insertByBo(GoodsBo bo) {
        Goods goods = MapstructUtils.convert(bo, Goods.class);
        validEntityBeforeSave(goods);
        boolean flag = baseMapper.insert(goods) > 0;
        if (goods != null) {
            if (flag) {
                for (GoodSku goodSku : goods.getGoodSkuList()) {
                    goodSku.setGoodId(goods.getId());
                }
            }
            goodSkuService.save(goods.getGoodSkuList());
        }
        return flag;
    }

    /**
     * 修改商品
     */
    @Override
    public Boolean updateByBo(GoodsBo bo) {
        Goods update = MapstructUtils.convert(bo, Goods.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(Goods entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除商品
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
