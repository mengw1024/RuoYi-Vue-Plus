package org.dromara.wms.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.dromara.wms.domain.bo.PurchaseOrderItemBo;
import org.dromara.wms.domain.vo.PurchaseOrderItemVo;
import org.dromara.wms.domain.PurchaseOrderItem;
import org.dromara.wms.mapper.PurchaseOrderItemMapper;
import org.dromara.wms.service.IPurchaseOrderItemService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 采购订单明细Service业务层处理
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@RequiredArgsConstructor
@Service
public class PurchaseOrderItemServiceImpl implements IPurchaseOrderItemService {

    private final PurchaseOrderItemMapper baseMapper;

    /**
     * 查询采购订单明细
     */
    @Override
    public PurchaseOrderItemVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询采购订单明细列表
     */
    @Override
    public TableDataInfo<PurchaseOrderItemVo> queryPageList(PurchaseOrderItemBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<PurchaseOrderItem> lqw = buildQueryWrapper(bo);
        Page<PurchaseOrderItemVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询采购订单明细列表
     */
    @Override
    public List<PurchaseOrderItemVo> queryList(PurchaseOrderItemBo bo) {
        LambdaQueryWrapper<PurchaseOrderItem> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<PurchaseOrderItem> buildQueryWrapper(PurchaseOrderItemBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<PurchaseOrderItem> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getBusinessTime() != null, PurchaseOrderItem::getBusinessTime, bo.getBusinessTime());
        lqw.eq(bo.getSupplierManagementId() != null, PurchaseOrderItem::getSupplierManagementId, bo.getSupplierManagementId());
        lqw.eq(bo.getSalesmanId() != null, PurchaseOrderItem::getSalesmanId, bo.getSalesmanId());
        lqw.eq(bo.getWarehouseId() != null, PurchaseOrderItem::getWarehouseId, bo.getWarehouseId());
        lqw.eq(StringUtils.isNotBlank(bo.getSkuCode()), PurchaseOrderItem::getSkuCode, bo.getSkuCode());
        lqw.eq(StringUtils.isNotBlank(bo.getColor()), PurchaseOrderItem::getColor, bo.getColor());
        lqw.eq(bo.getWeight() != null, PurchaseOrderItem::getWeight, bo.getWeight());
        lqw.eq(bo.getVolume() != null, PurchaseOrderItem::getVolume, bo.getVolume());
        lqw.eq(bo.getLength() != null, PurchaseOrderItem::getLength, bo.getLength());
        lqw.eq(bo.getWide() != null, PurchaseOrderItem::getWide, bo.getWide());
        lqw.eq(bo.getHigh() != null, PurchaseOrderItem::getHigh, bo.getHigh());
        lqw.eq(bo.getTradePrice() != null, PurchaseOrderItem::getTradePrice, bo.getTradePrice());
        lqw.eq(bo.getPurchasePrice() != null, PurchaseOrderItem::getPurchasePrice, bo.getPurchasePrice());
        return lqw;
    }

    /**
     * 新增采购订单明细
     */
    @Override
    public Boolean insertByBo(PurchaseOrderItemBo bo) {
        PurchaseOrderItem add = MapstructUtils.convert(bo, PurchaseOrderItem.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改采购订单明细
     */
    @Override
    public Boolean updateByBo(PurchaseOrderItemBo bo) {
        PurchaseOrderItem update = MapstructUtils.convert(bo, PurchaseOrderItem.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(PurchaseOrderItem entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除采购订单明细
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
