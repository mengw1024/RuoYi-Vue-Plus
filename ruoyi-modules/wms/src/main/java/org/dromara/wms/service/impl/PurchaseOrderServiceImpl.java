package org.dromara.wms.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.dromara.wms.domain.bo.PurchaseOrderBo;
import org.dromara.wms.domain.vo.PurchaseOrderVo;
import org.dromara.wms.domain.PurchaseOrder;
import org.dromara.wms.mapper.PurchaseOrderMapper;
import org.dromara.wms.service.IPurchaseOrderService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 采购订单Service业务层处理
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@RequiredArgsConstructor
@Service
public class PurchaseOrderServiceImpl implements IPurchaseOrderService {

    private final PurchaseOrderMapper baseMapper;

    /**
     * 查询采购订单
     */
    @Override
    public PurchaseOrderVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询采购订单列表
     */
    @Override
    public TableDataInfo<PurchaseOrderVo> queryPageList(PurchaseOrderBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<PurchaseOrder> lqw = buildQueryWrapper(bo);
        Page<PurchaseOrderVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询采购订单列表
     */
    @Override
    public List<PurchaseOrderVo> queryList(PurchaseOrderBo bo) {
        LambdaQueryWrapper<PurchaseOrder> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<PurchaseOrder> buildQueryWrapper(PurchaseOrderBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<PurchaseOrder> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getBusinessTime() != null, PurchaseOrder::getBusinessTime, bo.getBusinessTime());
        lqw.eq(StringUtils.isNotBlank(bo.getCode()), PurchaseOrder::getCode, bo.getCode());
        lqw.eq(bo.getSupplierManagementId() != null, PurchaseOrder::getSupplierManagementId, bo.getSupplierManagementId());
        lqw.eq(bo.getSalesmanId() != null, PurchaseOrder::getSalesmanId, bo.getSalesmanId());
        lqw.eq(bo.getWarehouseId() != null, PurchaseOrder::getWarehouseId, bo.getWarehouseId());
        lqw.eq(StringUtils.isNotBlank(bo.getStatus()), PurchaseOrder::getStatus, bo.getStatus());
        lqw.eq(bo.getGoodsTotalMoney() != null, PurchaseOrder::getGoodsTotalMoney, bo.getGoodsTotalMoney());
        lqw.eq(bo.getPurchaseFreight() != null, PurchaseOrder::getPurchaseFreight, bo.getPurchaseFreight());
        lqw.eq(bo.getOtherCost() != null, PurchaseOrder::getOtherCost, bo.getOtherCost());
        lqw.eq(bo.getAmountAdvance() != null, PurchaseOrder::getAmountAdvance, bo.getAmountAdvance());
        lqw.eq(StringUtils.isNotBlank(bo.getTrackNumber()), PurchaseOrder::getTrackNumber, bo.getTrackNumber());
        lqw.eq(bo.getTotalPurchaseQuantity() != null, PurchaseOrder::getTotalPurchaseQuantity, bo.getTotalPurchaseQuantity());
        lqw.eq(bo.getStorageQuantity() != null, PurchaseOrder::getStorageQuantity, bo.getStorageQuantity());
        lqw.eq(bo.getStorageAmount() != null, PurchaseOrder::getStorageAmount, bo.getStorageAmount());
        lqw.eq(bo.getShippedQuantity() != null, PurchaseOrder::getShippedQuantity, bo.getShippedQuantity());
        lqw.eq(StringUtils.isNotBlank(bo.getShippedStatus()), PurchaseOrder::getShippedStatus, bo.getShippedStatus());
        return lqw;
    }

    /**
     * 新增采购订单
     */
    @Override
    public Boolean insertByBo(PurchaseOrderBo bo) {
        PurchaseOrder add = MapstructUtils.convert(bo, PurchaseOrder.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改采购订单
     */
    @Override
    public Boolean updateByBo(PurchaseOrderBo bo) {
        PurchaseOrder update = MapstructUtils.convert(bo, PurchaseOrder.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(PurchaseOrder entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除采购订单
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
