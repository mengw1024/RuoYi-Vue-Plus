package org.dromara.wms.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.dromara.wms.domain.bo.SupplierBo;
import org.dromara.wms.domain.vo.SupplierVo;
import org.dromara.wms.domain.Supplier;
import org.dromara.wms.mapper.SupplierMapper;
import org.dromara.wms.service.ISupplierService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 供应商管理Service业务层处理
 *
 * @author Lion Li
 * @date 2024-01-12
 */
@RequiredArgsConstructor
@Service
public class SupplierServiceImpl implements ISupplierService {

    private final SupplierMapper baseMapper;

    /**
     * 查询供应商管理
     */
    @Override
    public SupplierVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询供应商管理列表
     */
    @Override
    public TableDataInfo<SupplierVo> queryPageList(SupplierBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<Supplier> lqw = buildQueryWrapper(bo);
        Page<SupplierVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询供应商管理列表
     */
    @Override
    public List<SupplierVo> queryList(SupplierBo bo) {
        LambdaQueryWrapper<Supplier> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<Supplier> buildQueryWrapper(SupplierBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<Supplier> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getCode()), Supplier::getCode, bo.getCode());
        lqw.like(StringUtils.isNotBlank(bo.getName()), Supplier::getName, bo.getName());
        lqw.eq(StringUtils.isNotBlank(bo.getType()), Supplier::getType, bo.getType());
        lqw.eq(StringUtils.isNotBlank(bo.getEmail()), Supplier::getEmail, bo.getEmail());
        lqw.eq(bo.getDue() != null, Supplier::getDue, bo.getDue());
        lqw.eq(bo.getAdvance() != null, Supplier::getAdvance, bo.getAdvance());
        lqw.eq(StringUtils.isNotBlank(bo.getProvince()), Supplier::getProvince, bo.getProvince());
        lqw.eq(StringUtils.isNotBlank(bo.getCity()), Supplier::getCity, bo.getCity());
        lqw.eq(StringUtils.isNotBlank(bo.getDistrict()), Supplier::getDistrict, bo.getDistrict());
        lqw.eq(StringUtils.isNotBlank(bo.getAddress()), Supplier::getAddress, bo.getAddress());
        lqw.eq(StringUtils.isNotBlank(bo.getLinkman()), Supplier::getLinkman, bo.getLinkman());
        lqw.eq(StringUtils.isNotBlank(bo.getLandLine()), Supplier::getLandLine, bo.getLandLine());
        lqw.eq(StringUtils.isNotBlank(bo.getPhone()), Supplier::getPhone, bo.getPhone());
        lqw.eq(StringUtils.isNotBlank(bo.getAlibabaSupplierCode()), Supplier::getAlibabaSupplierCode, bo.getAlibabaSupplierCode());
        return lqw;
    }

    /**
     * 新增供应商管理
     */
    @Override
    public Boolean insertByBo(SupplierBo bo) {
        Supplier add = MapstructUtils.convert(bo, Supplier.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改供应商管理
     */
    @Override
    public Boolean updateByBo(SupplierBo bo) {
        Supplier update = MapstructUtils.convert(bo, Supplier.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(Supplier entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除供应商管理
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
